// Server configuration

import { Static } from 'runtypes'
import { Config } from './config-type'

const mongoHost = process.env.SUBWAY_DB_HOST || 'localhost'
const mongoPort = process.env.SUBWAY_DB_PORT || '27017'
const mongoName = process.env.SUBWAY_DB_NAME || 'upchieve'
const mongoPass = process.env.SUBWAY_DB_PASS
const mongoUser = process.env.SUBWAY_DB_USER

let mongoConn
if (mongoPass) {
  mongoConn = `mongodb+srv://${mongoUser}:${mongoPass}@${mongoHost}/${mongoName}`
} else {
  mongoConn = `mongodb://${mongoHost}:${mongoPort}/${mongoName}`
}

let redisConnectionString
const redisHost = process.env.SUBWAY_REDIS_HOST || 'localhost'
const redisPort = process.env.SUBWAY_REDIS_PORT || '6379'
if (process.env.SUBWAY_REDIS_USE_TLS === 'true') {
  redisConnectionString = `rediss://:${process.env.SUBWAY_REDIS_PASSWORD}@${redisHost}:${redisPort}`
} else {
  redisConnectionString = `redis://${redisHost}:${redisPort}`
}

const bannedServiceProviderList =
  process.env.SUBWAY_BANNED_SERVICE_PROVIDERS || 'Example'
const bannedServiceProviders = bannedServiceProviderList.split(',')

let nodeEnv = process.env.NODE_ENV
if (nodeEnv !== 'dev' && nodeEnv !== 'staging' && nodeEnv !== 'production') {
  nodeEnv = 'dev'
}

const config: Static<typeof Config> = {
  NODE_ENV: nodeEnv,
  SSL_CERT_PATH: '',
  // set host to your public IP address to test Twilio voice calling
  host: process.env.SUBWAY_SERVER_HOST || 'localhost:3000',
  database: mongoConn,
  sessionSecret: process.env.SUBWAY_SESSION_SECRET || 'secret',
  sessionCookieMaxAge:
    parseInt(process.env.SUBWAY_SESSION_COOKIE_MAX_AGE) || 5184000000,
  saltRounds: 10,
  smtp: {
    host: process.env.SUBWAY_SMTP_HOST || 'smtp.mailtrap.io',
    port: parseInt(process.env.SUBWAY_SMTP_PORT) || 2525,
    secure: ((process.env.SUBWAY_SMTP_SECURE as unknown) as boolean) || false,
    user: process.env.SUBWAY_SMTP_USER || '',
    password: process.env.SUBWAY_SMTP_PASSWORD || ''
  },
  sendgrid: {
    apiKey: process.env.SUBWAY_SENDGRID_API_KEY || '',
    contactTemplate: 'd-e79546f380874c58965c163f45df2ef4',
    verifyTemplate: 'd-02281875a1cf4575bd3568e674faf147',
    resetTemplate: 'd-5005d2beb2ad49a883a10364f3e14b81',
    openVolunteerWelcomeTemplate: 'd-0e4406696376446da0d580b9c7e9a95a',
    partnerVolunteerWelcomeTemplate: 'd-eac2fdfaa8d740c8965ba8011102986b',
    studentWelcomeTemplate: 'd-dba390e0c99545d4b18135d869046f50',
    reportedSessionAlertTemplate: 'd-fe338f96339445279c3fa6580fabc286',
    referenceFormTemplate: 'd-122f9c9953144e62b1f66411b8e85723',
    approvedNotOnboardedTemplate: 'd-2c7a974fc7084a9eaaa152f3498cc99d',
    openReadyToCoachTemplate: 'd-09ec01c51be54c108b7fae4481acfd38',
    partnerReadyToCoachTemplate: 'd-d2ba9920683b4bd4a159f496a2346583',
    bannedUserAlertTemplate: 'd-7be8a21a59664f99a1f540e43c79b793',
    referenceFollowupTemplate: 'd-6b0d96c0cf15469db7a5e6ec655cf37a',
    rejectedPhotoSubmissionTemplate: 'd-c6d146a9eb4e44f6acc94a29a131d50d',
    rejectedReferenceTemplate: 'd-9afea0862a264bbb93ed6a2c074fd6b4',
    waitingOnReferencesTemplate: 'd-65bf8204d28746f58ad28a4aa92407c7',
    niceToMeetYouTemplate: 'd-8afee528e5184d8797c50c109d6b631b',
    weeklyHourSummaryEmailTemplate: 'd-19a5fbe8656249d2822c8bde1c2ab086',
    weeklyHourSummaryIntroEmailTemplate: 'd-4d8394e4da3847eabdfd23f257f7a8d3',
    onboardingReminderOneTemplate: 'd-9af291b6d0e94f7ebf0133be2c7176e3',
    onboardingReminderTwoTemplate: 'd-23681dc2dc0647e0bd7a57a6f328e00b',
    onboardingReminderThreeTemplate: 'd-d7ff9f33620842508de0030c84da9425',
    studentUseCasesTemplate: 'd-53b05a9788d044dab4c3e0f5e8a2250b',
    meetOurVolunteersTemplate: 'd-799e0195f78d4732a5adc36216a488f7',
    studentIndependentLearningTemplate: 'd-3167ea240a27471597d849cbae4b8437',
    studentGoalSettingTemplate: 'd-c5671abc76884180b6912729190b7078',
    studentFirstSessionCongratsTemplate: 'd-8c54307ace4a498f800185f0e540b8ea',
    volunteerQuickTipsTemplate: 'd-b85620ef95b443878a6aeca1e99c94ef',
    partnerVolunteerOnlyCollegeCertsTemplate:
      'd-031f89e788c2481ea912e5840e7d92e1',
    partnerVolunteerLowHoursSelectedTemplate:
      'd-476522cdd78e4c4ebc2af51a2086a640',
    volunteerFirstSessionCongratsTemplate: 'd-ebd561df99a7497d9401cec3f54ef23a',
    partnerVolunteerReferACoworkerTemplate:
      'd-f129a964b34446e3962433851459b17a',
    partnerVolunteerTenSessionMilestoneTemplate:
      'd-0447cf80536a430881262f8f92044b73',
    volunteerGentleWarningTemplate: 'd-5f7366fbde7841beb757b8694afdc6a4',
    volunteerInactiveThirtyDaysTemplate: 'd-e2c9917e22c24d72a187ff00a5eff5e9',
    volunteerInactiveSixtyDaysTemplate: 'd-659b7e8d08754ef58d9b6e594f748e19',
    volunteerInactiveNinetyDaysTemplate: 'd-1bb491dbb4a044f5a4cd9cd926eacf38',
    unsubscribeGroup: {
      newsletter: 12567,
      account: 12570,
      volunteerSummary: 14543
    },
    contactList: {
      students:
        process.env.SUBWAY_STUDENT_CONTACT_LIST ||
        '1111111a-111b-111c-111d-11111111111e',
      volunteers:
        process.env.SUBWAY_VOLUNTEER_CONTACT_LIST ||
        '1111111a-111b-111c-111d-11111111111e'
    }
  },
  logLevel: process.env.SUBWAY_LOG_LEVEL || 'debug',
  mail: {
    senders: {
      noreply: process.env.SUBWAY_NOREPLY_EMAIL_SENDER || 'example@example.org',
      support: process.env.SUBWAY_SUPPORT_EMAIL_SENDER || 'example@example.org',
      recruitment:
        process.env.SUBWAY_RECRUITMENT_EMAIL_SENDER || 'example@example.org',
      students:
        process.env.SUBWAY_STUDENTS_EMAIL_SENDER || 'example@example.org',
      volunteerManager:
        process.env.SUBWAY_VOLUNTEER_MANAGER_EMAIL_SENDER ||
        'example@example.org',
      studentOutreachManager:
        process.env.SUBWAY_STUDENT_OUTREACH_MANAGER_EMAIL_SENDER ||
        'example@example.org',
      corporatePartnershipsManager:
        process.env.SUBWAY_CORPORATE_PARTNERSHIPS_MANAGER_EMAIL_SENDER ||
        'example@example.org'
    },
    receivers: {
      contact:
        process.env.SUBWAY_CONTACT_EMAIL_RECEIVER || 'example@example.org',
      staff: process.env.SUBWAY_STAFF_EMAIL_RECEIVER || 'example@example.org',
      support:
        process.env.SUBWAY_SUPPORT_EMAIL_RECEIVER || 'example@example.org',
      recruitment:
        process.env.SUBWAY_RECRUITMENT_EMAIL_RECEIVER || 'example@example.org',
      students:
        process.env.SUBWAY_STUDENTS_EMAIL_RECEIVER || 'example@example.org',
      volunteerManager:
        process.env.SUBWAY_VOLUNTEER_MANAGER_EMAIL_RECEIVER ||
        'example@example.org',
      studentOutreachManager:
        process.env.SUBWAY_STUDENT_OUTREACH_MANAGER_EMAIL_RECEIVER ||
        'example@example.org',
      corporatePartnershipsManager:
        process.env.SUBWAY_CORPORATE_PARTNERSHIPS_MANAGER_EMAIL_RECEIVER ||
        'example@example.org'
    },
    people: {
      volunteerManager: {
        firstName: process.env.SUBWAY_VOLUNTEER_MANAGER_FIRST_NAME || '',
        lastName: process.env.SUBWAY_VOLUNTEER_MANAGER_LAST_NAME || ''
      },
      studentOutreachManager: {
        firstName: process.env.SUBWAY_STUDENT_OUTREACH_MANAGER_FIRST_NAME || '',
        lastName: process.env.SUBWAY_STUDENT_OUTREACH_MANAGER_LAST_NAME || ''
      },
      corporatePartnershipsManager: {
        firstName:
          process.env.SUBWAY_CORPORATE_PARTNERSHIPS_MANAGER_FIRST_NAME || '',
        lastName:
          process.env.SUBWAY_CORPORATE_PARTNERSHIPS_MANAGER_LAST_NAME || ''
      }
    }
  },
  client: {
    host: process.env.SUBWAY_CLIENT_HOST || 'localhost:8080'
  },
  socketsPort: Number(process.env.SUBWAY_SOCKETS_PORT) || 3001,

  volunteerPartnerManifestPath:
    process.env.SUBWAY_VOLUNTEER_PARTNER_MANIFEST_PATH ||
    'localManifests/volunteer.yaml',

  customPartnerVolunteerReport: process.env.SUBWAY_CUSTOM_PARTNER_REPORT || '',

  studentPartnerManifestPath:
    process.env.SUBWAY_STUDENT_PARTNER_MANIFEST_PATH ||
    'localManifests/student.yaml',

  // Sentry Data Source Name
  sentryDsn: process.env.SUBWAY_SENTRY_DSN || '',

  // Twilio Credentials
  accountSid: process.env.SUBWAY_TWILIO_ACCOUNT_SID || '',
  authToken: process.env.SUBWAY_TWILIO_AUTH_TOKEN || '',
  sendingNumber: process.env.SUBWAY_TWILIO_SENDING_NUMBER || '',
  twilioAccountVerificationServiceSid:
    process.env.SUBWAY_TWILIO_ACCOUNT_VERIFICATION_SERVICE_SID || '',

  notificationSchedule: [
    // Minute 1 (the time after a session request is made)
    1 * 60 * 1000,
    12 * 1000,
    12 * 1000,
    12 * 1000,
    12 * 1000,
    // Minute 2
    12 * 1000,
    12 * 1000,
    12 * 1000,
    12 * 1000,
    12 * 1000,
    // Minute 3
    12 * 1000,
    12 * 1000,
    12 * 1000,
    12 * 1000,
    12 * 1000,
    // Minute 6
    2 * 60 * 1000 + 12 * 1000,
    5 * 1000,
    5 * 1000,
    5 * 1000,
    5 * 1000,
    // Minute 7
    40 * 1000,
    5 * 1000,
    5 * 1000,
    5 * 1000,
    5 * 1000,
    // Minute 8
    40 * 1000,
    5 * 1000,
    5 * 1000,
    5 * 1000,
    5 * 1000
  ],
  // voice to use to render speech
  voice: 'man',

  workerQueueName: 'main',
  redisConnectionString,
  firebase: {
    projectId: Number(process.env.SUBWAY_FIREBASE_PROJECT_ID) || 123456789012
  },
  bannedServiceProviders: bannedServiceProviders,
  awsS3: {
    accessKeyId: process.env.SUBWAY_AWS_ACCESSKEY || 'ACCESSKEY123',
    secretAccessKey:
      process.env.SUBWAY_SECRET_ACCESS_KEY || 'SECRETACCESSKEY789',
    region: process.env.SUBWAY_AWS_REGION || 'us-east-2',
    photoIdBucket: process.env.SUBWAY_PHOTO_ID_BUCKET || 'photo-id-bucket',
    sessionPhotoBucket:
      process.env.SUBWAY_SESSION_PHOTO_BUCKET || 'session-photo-bucket'
  },
  unleashId: process.env.SUBWAY_UNLEASH_ID || 'djwdKPaf7s3oxMgDrRrd',
  unleashName: process.env.SUBWAY_UNLEASH_NAME || 'dev',
  unleashUrl:
    process.env.SUBWAY_UNLEASH_URL ||
    'https://gitlab.com/api/v4/feature_flags/unleash/23285197',
  vueAppUnleashUrl:
    process.env.VUE_APP_UNLEASH_URL ||
    'https://gitlab.com/api/v4/feature_flags/unleash/23285197',
  vueAppUnleashId: process.env.VUE_APP_UNLEASH_ID || 'djwdKPaf7s3oxMgDrRrd',
  vueAppUnleashName: process.env.VUE_APP_UNLEASH_NAME || 'dev',
  vueAppSentryEnv: process.env.VUE_APP_SENTRY_ENV || 'development',
  vueAppSentryDsn: process.env.VUE_APP_SENTRY_DSN || '',
  posthogToken: process.env.SUBWAY_POSTHOG_TOKEN || 'bogus',
  posthogPersonalApiToken:
    process.env.SUBWAY_POSTHOG_PERSONAL_API_TOKEN || 'bogus',
  zwibblerUrl:
    process.env.VUE_APP_ZWIBBLER_URL || '/static/js/zwibbler-demo.js',
  websocketRoot: process.env.VUE_APP_WEBSOCKET_ROOT || 'ws://localhost:3000',
  serverRoot: process.env.VUE_APP_SERVER_ROOT || 'http://localhost:3000',
  socketAddress: process.env.VUE_APP_SOCKET_ADDRESS || 'http://localhost:3001',
  mainWebsiteUrl:
    process.env.VUE_APP_MAIN_WEBSITE_URL || 'http://localhost:8080',
  newRelicBrowserAccountId: process.env.VUE_APP_NEW_RELIC_ACCOUNT_ID || 'bogus',
  newRelicBrowserTrustKey: process.env.VUE_APP_NEW_RELIC_TRUST_KEY || 'bogus',
  newRelicBrowserAgentId: process.env.VUE_APP_NEW_RELIC_AGENT_ID || 'bogus',
  newRelicApprovedOrigin:
    process.env.VUE_APP_NEW_RELIC_APPROVED_ORIGIN || 'bogus',
  newRelicBrowserLicenseKey:
    process.env.VUE_APP_NEW_RELIC_LICENSE_KEY || 'bogus',
  newRelicBrowserAppId: process.env.VUE_APP_NEW_RELIC_APP_ID || 'bogus',
  papercupsId: process.env.VUE_APP_PAPERCUPS_ID || 'bogus',
  vueDevtools: nodeEnv === 'dev',
  /**
   *
   * @note: DefaultAzureCredential() requires AZURE_CLIENT_ID, AZURE_TENANT_ID, and AZURE_CLIENT_SECRET.
   * https://github.com/Azure/azure-sdk-for-js/blob/master/sdk/identity/identity/README.md#environment-variables
   *
   * To combine multiple credential instances please see:
   * https://github.com/Azure/azure-sdk-for-js/blob/master/sdk/identity/identity/README.md#define-a-custom-authentication-flow-with-the-chainedtokencredential
   *
   **/
  whiteboardStorageAppId: process.env.AZURE_CLIENT_ID || 'bogus',
  whiteboardStorageTenantId: process.env.AZURE_TENANT_ID || 'bogus',
  whiteboardStorageSecret: process.env.AZURE_CLIENT_SECRET || 'bogus',
  whiteboardStorageSubscriptionId:
    process.env.SUBWAY_AZURE_WHITEBOARD_SUBSCRIPTION_ID || 'bogus',
  whiteboardStorageAccountName:
    process.env.SUBWAY_WHITEBOARD_STORAGE_ACCOUNT_NAME || 'bogus',
  whiteboardStorageContainer:
    process.env.SUBWAY_WHITEBOARD_STORAGE_CONTAINER || 'bogus',
  version: process.env.SUBWAY_VERSION || 'development'
}

module.exports = config
export default config
